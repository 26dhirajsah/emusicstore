<footer class="">
    <p class="float-right"><a href="#">Back to top</a></p>
    <p>&copy; 2017-2019 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
</footer>
</div><!-- /.container -->



<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>

<script>window.jQuery || document.write('<script src="<c:url value='/resources/js/jquery-3.5.0.min.js' />"><\/script>')</script>
<script src="<c:url value='/resources/js/bootstrap.bundle.min.js' />"> </script></body>

</html>
