<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@include file="/WEB-INF/views/fragments/header.jsp"%>
<div class="container " style="margin-top:90px;">

    <div class="" >
        <h1>Product Detail</h1>
        <p class="lead">Here is the Detail of the Product</p>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <img src="#" alt="image" style="width: 100%; height: 300px"/>
            </div>
            <div class="col-md-5">
                <h3>${product.productName}</h3>
                <p>${product.productDescription}</p>
                <p><strong>Manufacturer</strong> : ${product.productManufacturer}</p>
                <p><strong>Category</strong> : ${product.productCategory}</p>
                <p><strong>Condition</strong> : ${product.productCondition}</p>
                <p> ${product.productPrice} USD</p>
            </div>
        </div>
    </div>
<%@include file="/WEB-INF/views/fragments/footer.jsp"%>